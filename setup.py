from setuptools import setup, find_packages

setup(
    name='flaskCommon',
    version='0.0',
    packages=find_packages(),

    install_requires=[
        'Flask',
        'PyYAML',
        'redis',
    ],

    author='Jeffrey Jen',
    author_email='yihungjen@macrodatalab.com',
    description='My modules commonly used for flask + python',
    long_description='',
    license='Apache 2.0',
    keywords=[],
    url='https://bitbucket.org/yihungjen/flask-common.git',

    zip_safe=False
)
