from functools import wraps

class _to_dict(type):
    def __new__(meta, classname, supers, classdict):
        def to_dict(ref):
            rv = dict(ref.payload or ())
            rv['message'] = ref.message
            return rv
        classdict['to_dict'] = to_dict
        return type.__new__(meta, classname, supers, classdict)

class ServiceException(Exception):
    '''This is the root exception object we will use /
    if an error at our scope occurred.
    '''
    __metaclass__ = _to_dict

    status_code = 503

    def __init__(self, status_code=None, message='', payload=None):
        Exception.__init__(self)
        self.status_code = status_code or ServiceException.status_code
        self.message = message
        self.payload = payload

    def __str__(self):
        return '[%d: %s: %s]' % (self.status_code, self.message, self.payload)

class BadRequest(ServiceException):
    '''For ill formed request parameters or query args'''
    __metaclass__ = _to_dict

    status_code = 400

    def __init__(self, message='Bad Request', payload=None):
        ServiceException.__init__(self,
            BadRequest.status_code,
            message,
            payload
        )

class NotYetImplemented(ServiceException):
    '''For features under construction'''
    __metaclass__ = _to_dict

    status_code = 501

    def __init__(self, message='Not Available', payload=None):
        ServiceException.__init__(self,
            NotYetImplemented.status_code,
            message,
            payload
        )
