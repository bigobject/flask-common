from redis import Redis
from urlparse import urlsplit

import common.config as config

_redis_instance = None

def get_redis():
    global _redis_instance

    if _redis_instance is None:
        cfg = config.load()
        url = urlsplit(cfg.session.host)
        _redis_instance = Redis(url.hostname, url.port)

    return _redis_instance

def getexp(key, exp=None):
    p = get_redis().pipeline()
    p.get(key)
    if exp is not None:
        p.expire(key, exp)
    return p.execute()
