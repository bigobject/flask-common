from error import BadRequest
from flask import request
from functools import partial
from functools import wraps
from werkzeug.utils import ArgumentValidationError
from werkzeug.utils import validate_arguments

def sanitize_args(f=None, data_to_args=True, **kwargs):
    '''Method for preprocessing payload, arguemnts from request data to managed
    method.  In additional to one to one matching between function signature to
    payload keyword, allow user to specify specific variable name to map to when
    payload has ill-formed keyword as arg'''

    '''Provide a why to plugin the auth in the basic auth'''
    if f is None:
        return partial(
            sanitize_args,
            data_to_args=data_to_args,
            **kwargs
        )

    @wraps(f)
    def decorated(*args, **kwargs):
        '''parse query args into dict, then it maps one to one to the
        decorated function argument list'''

        arguments = args
        payload = {}

        payload.update(request.values.to_dict() or {})

        if decorated.data_to_args:
            payload.update(request.get_json() or {})
        else:
            payload['data'] = request.data

        payload.update(kwargs)

        try:
            payload.update({k: payload.get(k if k in payload else v) for k, v in decorated.capture})
        except KeyError as krr:
            raise BadRequest(message=str(krr))

        try:
            args, kwargs = validate_arguments(f,
                args=arguments,
                kwargs=payload
            )
        except ArgumentValidationError as avrr:
            raise BadRequest(message=str(avrr))
        else:
            return f(*args)

    decorated.capture = kwargs.items()
    decorated.data_to_args = data_to_args

    return decorated
