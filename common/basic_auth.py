from flask import request, Response
from functools import wraps
from functools import partial

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f=None, authcore=('', '')):
    '''Provide a why to plugin the auth in the basic auth'''
    if f is None:
        if isinstance(authcore, dict):
            authcore = (authcore['username'], authcore['password'])
        return partial(
            requires_auth,
            authcore=authcore
        )

    def check_auth(username, password):
        """This function is called to check if a username /
        password combination is valid.
        """
        u, p = authcore
        return username == u and password == p

    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)

    return decorated
