from flask import g

import logging
import config

__all__ = [ 'logger', 'init' ]

_loglevel = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL
}

_logger = None

class CustomAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        return '[%s] %s' % (g.log_token, msg), kwargs

def logger():
    global _logger

    if _logger is None:
        raise RuntimeError('Logger instance not initialized')

    return _logger

def init(app=None):
    global _logger

    if _logger is None:
        cfg = config.load()

        console = logging.StreamHandler()
        console.setFormatter(logging.Formatter(
            fmt=cfg.logfmt,
            datefmt=cfg.datefmt
        ))

        if app is not None:
            app.logger.addHandler(console)
            app.logger.setLevel(_loglevel.get(cfg.loglevel, logging.INFO))
            _logger = CustomAdapter(app.logger, None)
        else:
            _logger = logging.getLogger()
            _logger.addHandler(console)
            _logger.setLevel(_loglevel.get(cfg.loglevel, logging.INFO))
