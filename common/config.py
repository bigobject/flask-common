import env
import os

# designated parser for setting value
import yaml

class Config(object):
    parser = yaml

    @staticmethod
    def parse(cfg_settings):
        for k, v in cfg_settings.iteritems():
            if isinstance(v, dict):
                cfg_settings[k] = Config(cfg_settings[k])
            elif isinstance(v, str) or isinstance(v, unicode):
                value = v.split(':', 2)
                if '_env' in value:
                    if len(value) is 3:
                        default = value[2]
                    else:
                        default = ''
                    cfg_settings[k] = env.source(value[1], default)

    def __init__(self, cfg_settings=None):
        '''Load from "thing" as a file like object with "flavored" encoding'''

        if cfg_settings is None:
            self._config = {}
        elif isinstance(cfg_settings, str) or \
                isinstance(cfg_settings, unicode):
            try:
                self._config = Config.parser.load(cfg_settings)
            except:
                self._config = {}
        else:
            self._config = cfg_settings

        Config.parse(self._config)

    def __repr__(self):
        return repr(self._config)

    def __dir__(self):
        return self._config.keys()

    def keys(self):
        return self._config.keys()

    def __getattr__(self, key):
        '''Safety net for the entire config scope'''

        if key in self._config:
            return self._config[key]

        setting = env.source(key)
        if setting is None:
            raise RuntimeError('config variable %s not found' % key)
        else:
            self._config[key] = setting
            return self._config[key]

    def get(self, key, default=None):
        try:
            return self.__getattr__(key)
        except:
            return default

_config = None

def load(default='config.yml', report=False):
    global _config

    config_path = env.source('SETTINGS_CONFIG')
    if config_path is None:
        config_path = default

    flavor = env.source('SETTINGS_FLAVOR')

    if _config is None:
        try:
            cfg_f = open(config_path)
        except:
            config_path = os.path.join(
                os.path.dirname(__file__), '../config', 'config_sample.yaml')
            cfg_f = open(config_path)
        try:
            cfg_settings = yaml.load(cfg_f)
        except:
            raise RuntimeError('Invalid config content %s' % config_path)

        if flavor is None:
            _config = Config(cfg_settings)
        elif flavor not in cfg_settings:
            raise RuntimeError('Config flavor invalid %s' % flavor)
        else:
            _config = Config(cfg_settings[flavor])

    if report:
        print yaml.dump(_config, indent=4)

    return _config

def export(to_file=None):
    global _config

    if to_file is not None:
        with open(to_file, 'w+') as export_fp:
            yaml.dump(_config, export_fp)
    else:
        return yaml.dump(_config)
