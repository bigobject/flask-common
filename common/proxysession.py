from datetime import timedelta
from flask import after_this_request
from flask import request
from functools import partial
from werkzeug.datastructures import CallbackDict

import common.config as config
import common.myredis as myredis
import common.logconfig as logconfig
import common.status as codes
import pickle

cfg = config.load()

class ProxySession(object):

    class proxydict(CallbackDict):
        def __init__(self, initial=None, new=False):
            def on_update(self):
                self.modified = True
            CallbackDict.__init__(self, initial, on_update)
            self.new = new
            self.modified = False

    prefix = 'proxy:'
    serializer = pickle
    expiration = timedelta(**cfg.session.expiration)

    @staticmethod
    def query_session(resid):
        '''Utility method to get proxied resource'''

        proxyid = ProxySession.prefix + resid

        data = myredis.get_redis().get(proxyid)

        return ProxySession.serializer.loads(data) if data else None

    @staticmethod
    def begin_session(f=None, open_handle=None, **kwargs):
        '''Method for preprocessing an endpoint that is a endpoint based session.
        The take away for this is an point may be tied to a resource that has an
        expirable context'''

        if f is None:
            return partial(ProxySession.begin_session,
                open_handle=open_handle,
                **kwargs
            )

        def wrapper_f(*args, **kwargs):
            '''when invoked, parse for proxy session ID and process session'''

            logger = logconfig.logger()

            serializer = ProxySession.serializer
            exp = ProxySession.expiration
            proxyid = ProxySession.prefix + request.path.replace('/', ':')[1:]

            # Ask session store if the object is available
            # if so extend the life time on it
            try:
                data, status = myredis.getexp(proxyid, exp)
            except Exception as wrr:
                logger.warning(wrr)
                data, status = (None, None)
            else:
                data = serializer.loads(data) if data else data

            proxy_session = None

            # depending on the state of status, invoke registered open_handle
            if not status or not data:
                # forward to session open handle
                proxy_session = ProxySession.proxydict(open_handle(proxyid), new=True)
                # register session namespace NOTE: GC marker to not expire
                myredis.get_redis().set(proxyid, serializer.dumps(None))
            else:
                # already available, deserialize now
                proxy_session = ProxySession.proxydict(data)
                logger.debug('begin_session - session - %s', proxy_session)

            @after_this_request
            def write_through_session_with_expire(response):
                '''This request caused a new resource opened, so we set a
                new session on this proxy session'''
                logger.debug(proxy_session)
                if proxy_session.get('err', False):
                    logger.warning("proxy handling failed")
                    myredis.get_redis().expire(proxyid, timedelta(seconds=3))
                elif response.status_code in codes.ok and \
                        (proxy_session.modified or proxy_session.new):
                    # FIXME: should this happen if the request failed??
                    ret = myredis.get_redis().setex(
                        proxyid,
                        serializer.dumps(dict(proxy_session)),
                        exp
                    )
                    logger.info('begin_session - new session - %s', ret)
                return response

            # weee!!! go pew pew!!
            return f(proxy_session, *args, **kwargs)

        return wrapper_f
