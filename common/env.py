import os
import yaml

__all__ = [ 'load', 'source' ]

_DEFAULTS = {
    'GUNICORN_WORKERS': '4',
    'GUNICORN_MAX_REQUEST': '100',
    'GUNICORN_GRACEFUL_TIMEOUT': '3600',
    'GUNICORN_SILENT_TIMEOUT': '3600',
    'GUNICORN_WORKER_CLASS': 'gevent',
    'GUNICORN_ACCEESS_LOG_FILE': '"-"',
    'GUNICORN_ERROR_LOG_FILE': '"-"',
}

def load(environ_obj):
    _DEFAULTS.update(environ_obj)

def source(key, override=''):
    '''Helper method to capture baseline minimum settings from environment'''
    return yaml.load(
        os.environ.get(key, _DEFAULTS[key] if key in _DEFAULTS else override))
