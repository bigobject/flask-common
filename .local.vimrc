let g:ctrlp_custom_ignore = {
  \ 'dir':  '\(\.\?venv\|\.egg-info\)$',
  \ 'file': '\.\(pyc\|sw*\)$',
  \ }

