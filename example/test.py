#!/usr/bin/env python

from flask import Flask, Response

app = Flask(__name__)

from common.util import sanitize_args
from common.proxysession import ProxySession as proxy

import json

@app.route('/login')
@sanitize_args(resid='id', navdata='nav-data')
def some_random_method(resid, token, timestamp, navdata):
    print resid
    print token
    print timestamp
    print navdata
    return 'ping'

@app.route('/resoruces', methods=['POST'])
@sanitize_args
def provision(heroku_id):
    print heroku_id
    return 'pong'

@app.route('/upload/<string:document_id>', methods=['POST'])
@sanitize_args(data_to_args=False)
def upload(document_id, data):
    print document_id
    print data
    return 'ding'

@app.route('/bilibili', methods=['POST'])
@sanitize_args(user_id='heroku_id')
def bilibili(user_id):
    print user_id
    return 'dong'

def hello(rinst, sid, expire):
    print rinst
    print sid
    print expire

@app.route('/proxy/<string:user_id>', methods=['POST'])
@proxy.begin_session(open_handle=hello)
def proxy_stuff(user_id):
    print user_id
    return 'proooooooooooxy'

###############################################################################

import unittest

class FlaskrTestCase(unittest.TestCase):
    '''Test driver plan'''

    def setUp(self):
        app.config['TESTING'] = True
        self.app = app.test_client()

    def tearDown(self):
        pass

    #def test_some_random_method(self):
    #    self.app.get(('/login?timestamp=1413008290&app=myapp&id=9a4b10a2e4444'
    #    '5b96aea5c333c8d0842&nav-data=eyJhZGRvbiI6IllvdXIgQWRkb24iLCJhcHBuYW1'
    #    'lIjoibXlhcHAiLCJhZGRvbnMiOlt7InNsdWciOiJjcm9uIiwibmFtZSI6IkNyb24ifSx'
    #    '7InNsdWciOiJjdXN0b21fZG9tYWlucyt3aWxkY2FyZCIsIm5hbWUiOiJDdXN0b20gRG9'
    #    'tYWlucyArIFdpbGRjYXJkIn0seyJzbHVnIjoieW91cmFkZG9uIiwibmFtZSI6IllvdXI'
    #    'gQWRkb24iLCJjdXJyZW50Ijp0cnVlfV19&token=invalid&email=username%40exa'
    #    'mple.com'))

    # def test_heroku_provision_request(self):
    #     data = ('{"heroku_id":"app5784@kensa.heroku.com","plan":"test",'
    #             '"callback_url":"http://localhost:7779/callback/999",'
    #             '"logplex_token":null,"region":"amazon-web-services::us-east-1",'
    #             '"options":{}}')
    #     self.app.post('/resoruces',
    #         data=data,
    #         content_type='application/json',
    #         content_length=len(data),
    #         environ_base={
    #             'HTTP_USER_AGENT': 'Chrome',
    #             'REMOTE_ADDR': '127.0.0.1'
    #         }
    #     )

    # def test_content_body_as_payload(self):
    #     data = ('{"author": "Jeffrey Jen", "date": "01/01/2014", "content": '
    #             '"dlkfjslfghslghsldfhsdhfgsolghsdhfpsdgpsdhgf"}')
    #     self.app.post('/upload/123',
    #         data=data,
    #         content_type='application/json',
    #         content_length=len(data),
    #         environ_base={
    #             'HTTP_USER_AGENT': 'Chrome',
    #             'REMOTE_ADDR': '127.0.0.1'
    #         }
    #     )

    #def test_bilibili(self):
    #    data = json.dumps(dict(
    #        user_id='test@example.org',
    #        plan='test',
    #        options={}
    #    ))
    #    self.app.post('/bilibili',
    #        data=data,
    #        content_type='application/json',
    #        content_length=len(data),
    #        environ_base={
    #            'HTTP_USER_AGENT': 'Chrome',
    #            'REMOTE_ADDR': '127.0.0.1'
    #        }
    #    )

    def test_proxy(self):
        self.app.post('/proxy/213',
            environ_base={
                'HTTP_USER_AGENT': 'Chrome',
                'REMOTE_ADDR': '127.0.0.1'
            }
        )

if __name__ == '__main__':
    unittest.main()
